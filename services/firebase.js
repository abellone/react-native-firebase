import firebase from 'firebase/app';
import 'firebase/firestore';

var firebaseConfig = {
  apiKey: 'AIzaSyBisGCJGTSgMnH9j4xhNzNYX_MN1KMCzkI',
  authDomain: 'practica-react-native.firebaseapp.com',
  projectId: 'practica-react-native',
  storageBucket: 'practica-react-native.appspot.com',
  messagingSenderId: '680396029558',
  appId: '1:680396029558:web:56a9986ee2947335a8a957',
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export {db};
