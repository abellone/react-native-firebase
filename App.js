import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';
import {db} from "./services/firebase";

export default function App() {
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const [email, setEmail] = useState("");

  function saveNewUser() {
    db.collection("Usuarios").add({
      name: name,
      surname: surname,
      email: email,
    });

    setName("");
    setEmail("");
    setSurname("");
  };

  return (
    <View style={styles.container}>
      <Text style={styles.sectionTitle}>Registro</Text>
      <View >
        <TextInput style={styles.input} placeholder="Nombre" value={name}
        onChange={(e) => setName(e.target.value)} />
        <TextInput style={styles.input} placeholder="Apellido" value={surname}
        onChange={(e) => setSurname(e.target.value)} />
        <TextInput style={styles.input} placeholder="Email" value={email}
        onChange={(e) => setEmail(e.target.value)} />
        <TouchableOpacity onPress={saveNewUser} style={styles.button}>
          <Text style={styles.buttonText}>Crear Cuenta</Text>
        </TouchableOpacity>
      </View>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#412954',
  },
  sectionTitle: {
    fontSize: 50,
    margin: 30,
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  input: {
    height: 40,
    margin: 12,
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 5,
    paddingLeft: 10,
  },
  button: {
    height: 40,
    width: 300,
    backgroundColor: '#C25D00',
    display: "flex",
    alignSelf: "center",
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    marginTop: 5,
  },
});
